# == Class: rbapp
#
# Full description of class rbapp here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { rbapp:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class rbapp {

  # niginx

  package { 'epel-release':
    ensure => 'installed',
  }

  package { 'nginx':
    ensure  => 'installed',
    require => Package['epel-release'],
  }

  service { 'nginx':
    enable  => true,
    ensure  => 'running',
    require => File['/etc/nginx/nginx.conf'],
  }

  file { '/etc/nginx/nginx.conf':
    ensure  => file,
    mode    => '0644',
    source  => 'puppet:///modules/rbapp/nginx.conf',
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

 # unicorn

  file { "/etc/unicorn":
    ensure  => "directory",
    require => Package['nginx'],
  }

  file { "/var/run/unicorn":
    ensure  => "directory",
    require => Package['nginx'],
  }

  package { 'unicorn':
    ensure   => 'installed',
    provider => gem,
    require  => [ File['/etc/unicorn'], File['/var/run/unicorn'] ],
  }

  file { '/etc/systemd/system/rbapp-unicorn.service':
    ensure  => file,
    mode    => '0644',
    source  => 'puppet:///modules/rbapp/rbapp-unicorn.service',
    require => Package['unicorn'],
  }

  service { 'rbapp-unicorn':
    enable  => true,
    ensure  => 'running',
    require => File['/etc/systemd/system/rbapp-unicorn.service'],
  }

  file { '/etc/nginx/conf.d/rbappunicorn.conf':
    ensure => file,
    mode   => '0644',
    source => 'puppet:///modules/rbapp/rbappunicorn.conf',
    notify => Service['nginx'],
  }

  file { '/etc/unicorn/rbapp-unicorn.conf':
    ensure => file,
    mode   => '0644',
    source => 'puppet:///modules/rbapp/rbapp-unicorn.conf',
    notify => Service['rbapp-unicorn'],
  }

  # MySQL

  class { '::mysql::server':
    root_password           => 'MyPass123',
    remove_default_accounts => true,
  }

  mysql::db { 'rbapp_development':
    user     => 'rbapp_user',
    password => 'MyPass123',
  }

  # Our Rails App code

  vcsrepo { '/usr/src/rbapp':
    ensure   => latest,
    provider => git,
    source   => 'https://jotacepea@bitbucket.org/jotacepea/rbapp.git',
    revision => 'master',
    force    => true,
  }

  exec { "reload-rbapp-unicorn_service" :
    command     => 'systemctl restart rbapp-unicorn.service && \
                   /usr/local/bin/bundle exec /usr/local/bin/rake \
                   db:migrate --trace',
    path        => '/usr/bin',
    cwd         => '/usr/src/rbapp/',
    subscribe   => Vcsrepo['/usr/src/rbapp'],
    require     => Vcsrepo['/usr/src/rbapp'],
    refreshonly => true
  }

  # Install "Backup-Manager"

  package { 'backup-manager':
    ensure          => 'installed',
    install_options => '--enablerepo=epel-testing',
  }

  file { '/etc/backup-manager.conf':
    ensure  => file,
    source  => 'puppet:///modules/rbapp/backup-manager.conf',
  }

  cron { 'full_backup':
    command => '/usr/sbin/backup-manager --verbose > /var/log/backup-manager.log',
    user    => root,
    hour    => '*/1',
    minute  => 05
  }

  # DataDog HQ
  
  class { "datadog_agent":
    api_key             => "1ef66752cdc11615bd5cbde496f64",
    log_to_syslog       => false,
    puppet_run_reports  => true,
  }

  class { 'datadog_agent::integrations::mysql' :
    host => 'localhost',
    password => 'ThisIsTheP4ssw0rdDataDog193931',
    sock => '/var/lib/mysql/mysql.sock',
    user => 'datadog'
  }

  mysql_user{ 'datadog@localhost':
    ensure        => present,
    password_hash => '*294237D522C6554885BBA97835708856AC3E780D',
    require       => Class['mysql::server'],
  }

  mysql_grant{'datadog@localhost/*.*':
    user       => 'datadog@localhost',
    table      => '*.*',
    privileges => ['REPLICATION CLIENT'],
  }

}

